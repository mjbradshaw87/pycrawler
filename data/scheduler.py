
class Scheduler(object):
    """
    Scheduler object for the PyCrawler.  It will handle the movement and crawling of links.  BY DEFAULT the scheduler is
    only a python array.  Customization of the scheduler is permitted, and highly customizable, as long as you
    drop frontier links into frontier after processing.
    """
    def __init__(self, seeds):
        """
        Construtor cor the Scheduler class.
        :param seeds:
        :return:
        """
        # make data structure
        self.seeds = seeds
        self.frontier = []

    def set_frontier(self, param):
        """
        This sets a url to the frontier array.  IF you would like to add a custom algorithm. This is where you would send
        frontier links for processing.
        :param param: a frontier object (url, crawl_depth) to be processed
        :return:
        """
        self.frontier += param
        # print(self.frontier)

    def get_single_seed(self):
        """
        returns a single seed from the stack
        :return: (link, None) link is a seed url, None is crawl depth
        """
        # Todo: Raise exception
        return (self.seeds.pop(), None) if len(self.seeds) > 0 else None

    def check_seeds(self):
        """
        This checks if there are seed URLS left in the seed array.
        :return: bool, if there are URL seeds left
        """
        return len(self.seeds)

    def get_single_frontier(self):
        """
        This returns a single seed CURRENTLY UNDER CONSTRUCTION
        :return: (link, int)
        """
        # Todo: Raise exception
        return self.frontier.pop() if len(self.frontier) > 0 else None

    def check_frontier(self):
        """
        Returns if there is a frontier URL to return.
        :return: bool
        """
        return len(self.frontier)
