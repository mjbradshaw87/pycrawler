import os
import sys
import json
from crawler.const import *


def file_to_config(file_string):
    """
    This reads in a file of seeds and makes the beginning frontier out of them
    :param file_string: The file you want to get data from
    :return:
    """
    # read in file and put each seed into seed_array
    with open(file_string, 'r') as read_in:
        seed = read_in.read()

    # split by new line and return
    return seed


def print_fatal_error(error):
    """
    This method will print out all data needed to debug known errors
    :param error: The error code
    :return:
    """
    print(error_response(error))
    print('Exiting program...')
    sys.exit()


def read_config(config_json):
    """
    This method will read in the config file for the
    :param config_json:
    :return:
    """
    config_file = json.loads(file_to_config(config_json))
    return config_file


def print_out_fetched_page(fetched_page, output_path, worker_id):
    """
    This method will print out a page that was fetched.
    :param fetched_page: The fetched page
    :param output_path: The output path
    :param worker_id: the ID of the worker that fetched the page, a timestamp
    :return: void
    """
    try:
        # see if path exists
        if not os.path.exists(output_path):
            print('output path did not exist, trying to make it')
            os.makedirs(output_path)

        file_name = output_path + str(worker_id) + '.html'
        file = open(str(file_name), 'w')
        file.write(str(fetched_page.encode('utf8')))
        file.close()

    except Exception as ex:
        print_fatal_error(GENERAL_EXCEPTION_ERROR)
        print(ex)


def dict_to_json_file(dict, file_path, file_name):
    """
    This method will print out a dictionary to a file
    :param dict: The dictionary to dump
    :param file_path: The output file path
    :param file_name: the output file name
    :return: void
    """
    try:
        file = str(file_path) + str(file_name)
        with open(file, 'w') as outfile:
            json.dump(dict, outfile)

    except Exception as ex:
        print('There was an error printing out your dict')
        print(ex)
