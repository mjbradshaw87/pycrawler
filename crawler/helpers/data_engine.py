import threading


def threaded(func):
    """
    This decorator runs the decorated function in a separate thread, providing non-UI blocking methods
    :param func: A function/method to decorate
    :return: The decorated function
    """
    def wrapper(*args, **kwargs):
        threading.Thread(target=func, args=args, kwargs=kwargs).start()
    return wrapper
