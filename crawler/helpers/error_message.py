# error keys
GENERAL_EXCEPTION_ERROR = 0
INVALID_CONFIG_ERROR = 1
OUTPUT_FILE_ERROR = 2
CALLBACK_ERROR = 3
ROBOT_TXT_CANT_CRAWL = 4

# Error dict
__error_dict = {
    0: 'The program hit and unexpected error',
    1: 'Error reading the config file',
    2: 'There was an error making your output_file',
    3: 'No callback was passed',
    4: 'The URL"s robot.txt indicated it did not want the crawler to access the URL'
}


def __error_text(code=-1):
    """
    This method returns the error description that corresponds to the code.
    :param code: code of an error.
    :return: The error text (empty if not found).
    """
    return '' if code == -1 else __error_dict.get(code, '')


def error_response(code=-1, custom_message=''):
    """
    This method returns an HTTP JSON response with error text.
    :param code: code of an error.
    :param custom_message: custom text of an error (not required).
    :return: The HTTP JSON response.
    """
    if custom_message is not '':
        print(custom_message)
    else:
        text = __error_text(code)
        print(text)
