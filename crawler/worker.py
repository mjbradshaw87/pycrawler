import datetime
from urllib import parse

from crawler.fetcher import Fetcher
from crawler.const import *
import crawler.helpers.helper_io as io
from urllib.robotparser import RobotFileParser


class Worker(object):
    """
    The Worker class for the crawler, It will handle all aspects of getting info from a URL page
    this becomes a thread******
    """
    def __init__(self, config, filter, callback, is_frontier=True):
        """
        Constructor for the Worker class
        :param config: config file for the worker
        :param filter: copy of the filter made
        :param callback: the method to call back once the crawl is complete
        :return: void
        """
        self.config = config
        self.is_frontier = is_frontier
        self.fetcher = Fetcher()
        self.filter = filter
        self.worker_id = str(datetime.datetime.now())

        if not hasattr(callback, '__call__'):
            raise Exception(error_response(CALLBACK_ERROR))

        self.callback = callback

    def test(self, url):
        """
        Test method to test if the worker will fetch the page. THIS IS FOR DEBUG ONLY
        :param url: test url
        :return: void
        """
        fetched_page = self.fetcher.fetch_page(url)
        if len(fetched_page):
            print('Test Fetch works', self.worker_id)

    def get_node(self, url_info):
        """
        This method will get a node to add to the crawler node.  The node is a dictionary, it contains all of the things
         that the crawler is looking for.
        :param url_info: The url info object form the scheduler (link, crawl_depth)
        :return: Node, a dict of important search criteria
        """
        url, crawl_depth = url_info

        # see if we can crawl a url
        if not self.can_crawl(url=url):
            print('sry')
            return None

        # try to get the page
        requested_page = self.fetcher.fetch_page(url)

        # if there was a timeout return none
        if requested_page is None:
            return None

        soup = self.filter.get_soup(fetched_page=requested_page)
        links = self.filter.get_links(soup, is_frontier=self.is_frontier)
        attributes = self.filter.try_get_classes(soup=soup, is_frontier=self.is_frontier)
        tables = self.filter.try_get_table(soup=soup, is_frontier=self.is_frontier)

        if self.config[PAGE_PRINT_KEY]:
            # print the page to an output file
            io.print_out_fetched_page(
                fetched_page=soup,
                output_path=str(self.config[OUTPUT_PATH_KEY]),
                worker_id=self.worker_id
            )
            print(self.config[OUTPUT_PATH_KEY])

        return_dict = {
            URL_KEY: url,
            CRAWL_DEPTH_KEY: 0 if not self.is_frontier else crawl_depth,
            LINKS_KEY: links,
            ATTRIBUTES_KEY: attributes,
            TABLES_KEY: tables,
            SOUP_KEY: soup
        }
        self.callback(return_dict, self.worker_id)

    def can_crawl(self, url):
        """

        :param url:
        :return:
        """
        rp = RobotFileParser()

        # parse string for a robot text
        parsed_url = parse.urlparse(url=url)
        robot_string = str(parsed_url.scheme) + '://' + str(parsed_url.netloc)

        rp.set_url(robot_string + '/robot.txt')
        rp.read()
        return rp.can_fetch(useragent=USER_AGENT, url=url)