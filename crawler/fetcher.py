import requests
from bs4 import BeautifulSoup


class Fetcher(object):
    """
    This is the fetcher class for the crawler.  It will get a url and return it to a worker
    """
    def __init__(self):
        """
        Initializes the Fetcher class
        :return: void
        """

    def fetch_page(self, url):
        """
        This method will fetch a page and return it as a string
        :param url: The page you want to fetch
        :return:
        """
        try:
            page = requests.get(url, timeout=5)
            return page.text
        except Exception as ex:
            print(ex)
            return None

    def make_document(self, html_string):
        """

        :param html_string:
        :return:
        """
        return BeautifulSoup(html_string)
