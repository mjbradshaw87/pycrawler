CRAWLER_NAME = 'PyCrawler-scholar'
CRAWLER_VERSION = 'v0.1'
USER_AGENT = CRAWLER_NAME + CRAWLER_VERSION

# Configuration
CONFIGURATION_KEY = 'configuration'
THREADED_KEY = 'threaded'
SEEDS_KEY = 'seeds'
OUTPUT_PATH_KEY = 'output_path'

FILTER_KEY = 'filter'
COMMON_INFO_KEY = 'common_info'
SEED_INFO_KEY = 'seed_info'
FRONTIER_INFO_KEY = 'frontier_info'
A_TAG_LINKS_KEY = 'a_tag_links'
GET_LINK_KEY = 'get_link'
A_LINK_WITH_ATTR = 'a_link_with_attr'
TABLES_KEY = 'tables'
TD_ATTRIBUTES = 'td_attributes'
GET_TEXT_KEY = 'get_text'
SPECIAL_LINKS_KEY = 'special_links'
KEYWORDS_KEY = 'keywords'
ATTRIBUTES_KEY = 'attributes'
CLASS_KEY = 'class'
IDS_KEY = 'ids'
SOUP_KEY = 'soup'

WORKER_KEY = 'worker'
PAGE_PRINT_KEY = 'page_print'
MAP_REDUCE_KEY = 'map_reduce'

SCHEDULER_KEY = 'scheduler'
CRAWL_DEPTH_KEY = 'crawl_depth'


# node keys
URL_KEY = 'url'
LINKS_KEY = 'links'
LINK_KEY = 'link'
TEXT_KEY = 'text_key'
PAGE_KEY = 'page_key'

