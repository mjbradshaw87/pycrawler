from crawler.parser import Parser
from crawler.const import *


class Filter(object):
    """
    This is the Filter class for the crawler, it will ask the parser for all necessary information needed
    to do the crawl.  It controls the Parser class.
    """
    def __init__(self, config_data):
        """

        :param config_data: The JSON config data, "filter" section
        :return: void
        """
        self.config_data = config_data
        self.parser = Parser()

    def get_soup(self, fetched_page):
        """
        This method will send a fetched page to the parser to get a BeautifulSoup Object
        :param fetched_page: The URL we fetched to make a BeautifulSoup Object
        :return: a BeautifulSoup Object
        """
        return self.parser.get_soup(fetched_page)

    def get_links(self, soup, is_frontier):
        """
        This method will get a list of links from the soup document
        :param is_frontier: bool,
        :param soup: beautfulsoup object
        :return: dictionary of page { LINK: link, TEXT: text },
        """
        all_page_links = []

        # check to see if this is a frontier crawl
        if is_frontier:
            # check to see if we are collecting strict <a> links
            if self.config_data[FRONTIER_INFO_KEY][A_TAG_LINKS_KEY] is not None:
                pass

            # check to see if we are crawling links with attribute
            if self.config_data[FRONTIER_INFO_KEY][A_LINK_WITH_ATTR] is not None:
                # first get the attributes that hold links
                instances = []
                for attribute in self.config_data[FRONTIER_INFO_KEY][A_LINK_WITH_ATTR][ATTRIBUTES_KEY]:
                    instances += self.parser.scrape_attr(soup=soup, attr=attribute)

                # next get <a> links in each attribute and text
                for instance in instances:
                    link = self.parser.scrape_links(instance)
                    text = self.parser.scrape_text(attr=instance)
                    link = self.parser.scrape_text(attr=instance)
                    all_page_links.append((link, text))

            # check to see if we are collecting special links
            if self.config_data[FRONTIER_INFO_KEY][SPECIAL_LINKS_KEY] is not None:
                pass

        else:
            # check to see if we are collecting <a> links
            if self.config_data[SEED_INFO_KEY][A_TAG_LINKS_KEY] is not None:
                pass

            # check to see if we are collecting <a> with attr
            if self.config_data[SEED_INFO_KEY][A_TAG_LINKS_KEY] is not None:
                pass

            # check to see if we are collecting special links
            if self.config_data[SEED_INFO_KEY][SPECIAL_LINKS_KEY] is not None:
                # first get all the instances of EACH attr
                instances = []
                for attribute in self.config_data[SEED_INFO_KEY][SPECIAL_LINKS_KEY][ATTRIBUTES_KEY]:
                    instance = self.parser.scrape_attr(soup=soup, attr=attribute)
                    instances = instances + instance

                # then for each instance get the hidden
                for instance in instances:
                    link = self.parser.scrape_text(attr=instance)
                    all_page_links.append((link, None))

        # todo: change this later
        # links = helper_url.normalize_links(page_links_scraped, url=url)
        return all_page_links

    def try_get_classes(self, soup, is_frontier):
        """
        gets all classes of some attr
        :param is_frontier: bool, tells the class if this is a frontier URL or seed URL.  determines what data is collected
        :param soup: BeautifulSoup object
        :return: { classes: [], ids : [] }
        """
        classes = []
        ids = []

        # TODO: take out the different class and id attributes and bring to just one atributes key
        # see if there is any common attributes to get
        if self.config_data[COMMON_INFO_KEY][ATTRIBUTES_KEY] is not None:
            for attribute in self.config_data[COMMON_INFO_KEY][ATTRIBUTES_KEY]:
                classes.extend(self.parser.scrape_attr(soup=soup, attr=attribute))

        # check to see if we are in the frontier
        if is_frontier:
            # see if there are attributes to get from the frontier
            if self.config_data[FRONTIER_INFO_KEY][ATTRIBUTES_KEY] is not None:
                # if so iterate through each attribute and append to our classes list
                for attribute in self.config_data[FRONTIER_INFO_KEY][ATTRIBUTES_KEY][CLASS_KEY]:
                    classes.extend(self.parser.scrape_attr(soup=soup, attr=attribute))

                for attribute in self.config_data[FRONTIER_INFO_KEY][ATTRIBUTES_KEY][IDS_KEY]:
                    ids.extend(self.parser.scrape_attr(soup=soup, attr=attribute))

        # if in seed
        else:
            # see if there are seed attributes to get
            if self.config_data[SEED_INFO_KEY][ATTRIBUTES_KEY] is not None:
                # setup params
                class_param = self.config_data[SEED_INFO_KEY][ATTRIBUTES_KEY][CLASS_KEY]
                id_param = self.config_data[SEED_INFO_KEY][ATTRIBUTES_KEY][IDS_KEY]

                # iterate through each attribute and append to your classes list
                if class_param is not None:
                    for attribute in class_param:
                        classes.extend(self.parser.scrape_attr(soup=soup, attr=attribute))

                if id_param is not None:
                    for attribute in id_param:
                        ids.extend(self.parser.scrape_attr(soup=soup, attr=attribute))

        return {
            CLASS_KEY: classes,
            IDS_KEY: ids
        }

    def try_get_attr_text(self, soup, attr):
        """
        this gets the text inside an attribute
        :param soup: the BeautifulSoup object
        :param attr: the attribute we want text from
        :return: list of text for each instance of attribute
        """
        return self.parser.scrape_attr_text(soup=soup, attr=attr)

    def try_get_table(self, soup, is_frontier):
        """
        This method will get all table rows.  It defaults to just table elementes but can filter if you
        provide an attribute to filter
        :param soup: The beautiful soup document
        :param is_frontier: bool, if this is a frontier link or not
        :return:
        """
        # setup params
        common_table = self.config_data[COMMON_INFO_KEY][TABLES_KEY]
        frontier_table = self.config_data[FRONTIER_INFO_KEY][TABLES_KEY]
        seed_table = self.config_data[SEED_INFO_KEY][TABLES_KEY]
        tr_data = []

        # TODO: implement for common
        # first check to see if we need to get any common tables
        if common_table is not None:
            pass

        # TODO: implement for frontier
        # see if we are crawling frontier
        if is_frontier:
            pass

        # if seed
        else:
            # see if we need to get table rows for seeds
            if seed_table is not None:
                attributes = self.config_data[SEED_INFO_KEY][TABLES_KEY][TD_ATTRIBUTES]
                tr_instances = self.parser.scrape_table_rows(soup=soup)

                # collect the data
                if attributes is not None:
                    # iterate through each instance of tr and get all data of classes and ids
                    for instance in tr_instances:
                        # check for td with attributes
                        for attr in attributes:
                            tr_attributes = self.parser.scrape_attr(soup=instance, attr=attr)

                            tr_data.extend((str(attr), str(tr_attributes)))

                # if no attributes, just return row
                else:
                    for instance in tr_instances:
                        tr_data.extend(instance)
        return tr_data
