from bs4 import BeautifulSoup


class Parser(object):
    """
    This class parses a BeautifulSoup object and returns the parsed data
    """
    def __init__(self):
        """
        Constructor for the Parser class
        :return: void
        """

    def get_soup(self, fetched_page):
        """
        This method take a fetched page and turns it into a BeautifulSoup object
        :param fetched_page: the URL to turn into a BeautifulSoup Object
        :return: a BeautifulSoup Object
        """
        return BeautifulSoup(fetched_page, 'lxml')

    def scrape_links(self, soup):
        """
        This method will return all of the links on a page
        :param soup: a BeautifulSoup Object
        :return: list of all links on a page
        """
        links = []
        for link in soup.find_all('a'):
            links.append(link.get('href'))
        return links

    def scrape_attr(self, soup, attr):
        """
        needs work
        :param soup:
        :param attr:
        :return:
        """
        instance_text = []
        for class_instance in soup.find_all(attrs=str(attr)):
            if not None:
                instance_text.append(class_instance)
        return instance_text

    def scrape_attr_text(self, soup, attr):
        """
        This method will look for attributes on a BeautifulSoup object and return the text in those attributes
        :param soup: the beautifulsoup object
        :param attr: the attribute to get text from
        :return:list of attribute text, each cell is an instance
        """
        instance_text = []
        for class_instance in soup.find_all(attrs=str(attr)):
            if not None and len(str(class_instance.get_text())) > 1:
                instance_text.append(class_instance.get_text())
        return instance_text

    def scrape_text(self, attr):
        """
        This method will get the text of an attribute.
        :param attr: The attribute you want text from
        :return: a string that is the text inside an HTML tag with a certain attribute
        """
        return attr.get_text()

    def scrape_table_rows(self, soup):
        """
        This method will scrape and remove all of the 'tr' tags from the soup page
        :param soup: the BeautifulSoup object
        :return: list of tr data
        """
        return soup.find_all('tr')
