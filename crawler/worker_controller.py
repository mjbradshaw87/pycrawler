from crawler.worker import Worker
from crawler.helpers.data_engine import threaded
from crawler.const import *


class WorkerController(object):
    """
    This is the controller for a crawler
    """
    def __init__(self, config, scheduler, filter):
        """

        :param config: The config file the user entered
        :param scheduler: the scheduler object to hold links, seeds
        :param filter: the filter object to gather the data needed
        :return: void
        """
        self.config = config
        self.filter = filter
        self.scheduler = scheduler

    def crawl(self):
        """
        This is a single threaded crawl.  It handles the creation and execution of the worker.
        :return: void, the crawl is done
        """
        # crawl all seeds
        while self.scheduler.check_seeds():
            self.non_threaded_worker(
                seed=self.scheduler.get_single_seed(),
                is_frontier=False
            )

        # crawl all frontier urls
        while self.scheduler.check_frontier():
            self.non_threaded_worker(
                seed=self.scheduler.get_single_frontier(),
                is_frontier=True
            )

    def non_threaded_worker(self, seed, is_frontier=True):
        """
        This method is a single thread worker
        :param is_frontier: Bool, lets the worker know if this is a frontier URL or seed URL
        :param seed: the URL tor crawl,  could be seed or frontier URL
        :return: void
        """
        def single_thread_callback_seed(result, worker_id):
            """
            callback for single tread app.  Handles the routing for data
            :param worker_id: The creation timestamp for the worker object
            :param result: The dictionary of crawl information
            :return: void
            """
            pass
            # # update user
            # print('worker {0} crawled {1}'.format(worker_id, str(result[URL_KEY])))
            #
            # if self.config[WORKER_KEY][CRAWL_DEPTH_KEY] > 0:
            #     # get the tuples list and break then down
            #     links_info = result[LINKS_KE Y]
            #     frontier_links = []
            #
            #     # route the links and text
            #     for tup in links_info:
            #         link, text = tup
            #
            #         # make new data structures
            #         # TODO: extend functionality
            #         if len(link) > 1:
            #             frontier_data = (link, result[CRAWL_DEPTH_KEY])
            #             frontier_links.append(frontier_data)
            #
            #     # send to scheduler
            #     self.scheduler.set_frontier(param=frontier_links)
            #
            # else:
            #     print(result)

        def single_thread_callback_frontier(result, worker_id):
            """
            callback for single tread app
            :param worker_id: The creation timestamp for the Worker object
            :param result: The dictionary of crawl information
            :return: void
            """
            pass
            # # update user
            # print('worker {0} crawled {1}'.format(worker_id, str(result[URL_KEY])))
            # # if

        worker = Worker(
            filter=self.filter,
            callback=single_thread_callback_frontier if is_frontier else single_thread_callback_seed,
            is_frontier=is_frontier,
            config=self.config[WORKER_KEY]
        )
        worker.get_node(url_info=seed)

    def crawl_threaded(self):
        """
        This method will crawl all the links in the seeds and the frontier
        :return: void worker calls callback
        """
        # crawl the initial seeds
        pass

    @threaded
    def thread_worker(self, seed, callback=None):
        """
        this makes it multiple workers
        :param callback: the callback variable
        :param seed: the url to crawl
        :return: void, calls callback
        """
        def callback_seeds_threaded(self, result, worker_id):
            """
            this is the call back after the worker crawls seeds
            :param result: the seed dictionary
            :param worker_id: timestamp of worker creation
            :return:
            """
            print(worker_id)
            # if result is not None:
            #     pass  # for testing
            #     # for link in result:
            #         # self.frontier.append(link)
            # else:
            #     print('callback error')
            #     print('adding seed back into the queue')

        def callback_frontier_threaded(self, result, worker_id):
            """
            This is the callback after the worker crawls the frontier
            :param result: the frontier dictionary
            :param worker_id: timestamp of worker creation
            :return: void
            """
            pass
        pass
