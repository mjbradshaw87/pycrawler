PYCRAWLER README

Welcome to PyCrawler README.  Basic PyCrawler operations will be presented.

--- WHAT IS PYCRAWLER? ---

PyCrawler is a highly modular web crawler.


--- FIRST TIME SETUP ---

If you plan on doing any work with PyCrawler, the first thing you need to do is rename your crawler.  This is done in
the 'const.py' file.  On the top line, you will see 'CRAWLER_NAME', change the string of the variable.  It is important
for the program because it will check robots.txt against this name.


--- JSON FORMAT ---

The config file for the crawler is a JSON file.

{
  "configuration": {
    "threaded": false,
    "seeds": [
      "https://google.com",
      "https://espn.go.com",
      "https://nbc.com"
    ]
  },

  "filter": {
    "common_info": {
      "keywords": null,
      "attributes": null,
      "tables": null
    },

    "seed_info": {
      "a_tag_links": null,
      "a_link_with_attr": null,
      "special_links": null,
      "keywords": null,
      "attributes": null,
      "tables": null
    },

    "frontier_info": {
      "a_tag_links": null,
      "a_link_with_attr": null,
      "special_links": null,
      "keywords": null,
      "attributes": null,
      "tables": null
    }
  },

  "worker": {
    "page_print": true,
    "output_path": "/Users/michaelbradshaw/Desktop/crawlers/PyCrawler/",
    "map_reduce": false,
    "crawl_depth": 0
  },

  "scheduler": false
}
** Documentation of each parameter from top to bottom **

configuration:
@value: dictionary
@description: handles initial setup of PyCrawler

    threaded:
    TODO: make multithreaded
    @value: bool
    @description: tells PyCrawler if you want a multithreaded crawl

    seeds:
    @value: list
    @description: This is a list of seeds to start the crawl.  One URL per index

    output_path:
    @value: string
    @description: This tells PyCrawler where to output your results.  It will make a folder inside this
    path that will be the run folder

    output_file:
    @value: string
    @description: This tells PyCrawler the file name you want run information in.

filter:
@value: dictionary
@description: handles the configuration for the filter

    common_info:
    @value: dictionary
    @description: This is information common to both the seeds and the frontier.  Any stored information will be collected
    on both the seed crawl and the frontier crawl

        keywords:
        TODO: make
        @value: list
        @description: PyCrawler will search EACH DOM element and look for keywords in this list.  If any keywords are found
        it will return the text of the tag the keyword was found in.  Any keyword in this list will be searched for in
        both the seeds and the frontier.

        attributes:
        @value: dictionary
        @description: PyCrawler will search the DOM for any tags that have the provided attributes. Any attribute in this
        list will be searched for in both the seeds and the and the frontier.

            class:
            @value: list
            @description: A list of class attributes that PyCrawler will search for.  One class per index. Any class
            in this list will be searched for in both the seeds and the and the frontier.

            ids:
            @value: list
            @description: A list of ids attributes that PyCrawler will search for.  One id per index. Any id
            in this list will be searched for in both the seeds and the and the frontier.

    seed_info:
    @value: dictionary
    @description:

        a_tag_links:
        @value: bool
        @description:

        special_links:
        @value: list
        @description: attrs of special links you want collected

    frontier_info



