import sys
from crawler.const import *
import crawler.helpers.helper_io as helper
from crawler.filter import Filter
from data.scheduler import Scheduler
from crawler.worker_controller import WorkerController


class CrawlerMain(object):
    """
    Entry point for the python crawler.  This class handles all the setup of the run.
    """
    def __init__(self):
        """
        constructor for the CrawlerMain
        :return:
        """
        self.config = None
        self.seeds = []
        self.output_path = None

        self.filter = None
        self.scheduler = None

    def start(self, args):
        """

        :param args:
        :return:
        """
        print('Checking input...')
        try:
            self.load_params(args)
        except Exception:
            helper.print_fatal_error(INVALID_CONFIG_ERROR)

        self.scheduler = Scheduler(seeds=self.seeds)
        # self.router = Router(config_data=self.config[WORKER_KEY], scheduler=self.scheduler)

        # check decided to go threaded or not threaded TODO: not threaded for now
        worker_controller = WorkerController(
            config=self.config,
            scheduler=self.scheduler,
            filter=self.filter,
        )
        if self.config[CONFIGURATION_KEY][THREADED_KEY]:
            print('Multi-threaded not supported')
        else:
            worker_controller.crawl()
            print('crawl complete')

    def load_params(self, args):
        """
        This method will set the crawl
        :param args: passed arguments
        :return: bool, if parameters were loaded correctly
        """
        print('Setting up the crawl...')
        # check harvest case
        if len(args) == 2:
            self.config = helper.read_config(args[1])
            self.seeds = self.config[CONFIGURATION_KEY][SEEDS_KEY]
            self.output_path = self.config[WORKER_KEY][OUTPUT_PATH_KEY]

            # make filter once basic requirements are filled
            self.filter = Filter(config_data=self.config[FILTER_KEY])
            return True
        else:
            return False


crawler = CrawlerMain()
crawler.start(args=sys.argv)
